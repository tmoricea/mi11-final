/*----------------------------------------------------------------------------*
 * fichier : noyau_test.c                                                     *
 * programme de test du noyaut                                                *
 *----------------------------------------------------------------------------*/

#include <stdint.h>
#include <stdlib.h>

#include "../TP1-2/noyau.h"
#include "../Communs/stm32h7xx.h"
#include "../Communs/serialio.h"
#include "../Communs/TERMINAL.h"
#include "../Communs/FIFO.h"


TACHE	tacheA(void);
TACHE	Prod(void);
TACHE	Cons(void);


#define MAX_CARA_LIGNE 80

uint16_t pos_x = 1;
uint16_t pos_y = 6;
FIFO f;


TACHE	tacheA(void)
{
  SET_CURSOR_POSITION(3,1);
  puts("------> EXEC tache A");

  fifo_init(&f);
  active(cree(Prod));
  active(cree(Cons));
  fin_tache();
}



TACHE Prod( void )
{
	uint16_t d = 0;
	uint64_t j;

	SET_CURSOR_POSITION(4,1);
	puts("------> DEBUT tache PROD\n");
	SAVE_CURSOR_POSITION();

	dort();

	while(d < 100)
	{
		for (j=0; j<2500000L; j++);
		_lock_();
		if (!fifo_ajoute(&f, d))
		{
			if (pos_x > MAX_CARA_LIGNE) {pos_x = 1; pos_y = pos_y+1;}
			SET_CURSOR_POSITION(pos_y,pos_x);
			SET_BACKGROUND_COLOR(33);
			SET_FONT_COLOR(236);
			printf(" Pdort");
			pos_x = pos_x + 6;
			_unlock_();
			dort();
		}
		else
		{
			if (pos_x > MAX_CARA_LIGNE) {pos_x = 1; pos_y = pos_y+1;}
			SET_CURSOR_POSITION(pos_y,pos_x);
			SET_BACKGROUND_COLOR(46);
			SET_FONT_COLOR(236);
			printf(" P%4d", d);
			pos_x = pos_x + 6;
			d++;
			if (f.fifo_taille == 1)
			{
				if (pos_x > MAX_CARA_LIGNE) {pos_x = 1; pos_y = pos_y+1;}
				SET_CURSOR_POSITION(pos_y,pos_x);
				SET_BACKGROUND_COLOR(75);
				SET_FONT_COLOR(236);
				printf(" PrevC");
				pos_x= pos_x + 6;
				reveille(2);
			}
			_unlock_();
		}
	}
	_lock_();
	printf(" Fin du décompte");
	pos_x= pos_x + 16;
	_unlock_();
	fin_tache();
}

TACHE Cons( void )
{
	uint8_t d;
	uint64_t j;

	SET_CURSOR_POSITION(5,1);
	puts("------> DEBUT tache CONS\n");
	SAVE_CURSOR_POSITION();

	reveille(1);

	while(1)
	{
		for (j=0; j<10000000L; j++);
		for (j=0; j<5; j++)
		{
			_lock_();
			if (!fifo_retire(&f, (uint8_t *) &d))
			{
				if (pos_x > MAX_CARA_LIGNE) {pos_x = 1; pos_y = pos_y+1;}
				SET_CURSOR_POSITION(pos_y,pos_x);
				SET_BACKGROUND_COLOR(220);
				SET_FONT_COLOR(236);
				printf(" Cdort");
				pos_x = pos_x + 6;
				_unlock_();
				dort();
			}
			else
			{
				if (pos_x > MAX_CARA_LIGNE) {pos_x = 1; pos_y = pos_y+1;}
				SET_CURSOR_POSITION(pos_y,pos_x);
				SET_BACKGROUND_COLOR(196);
				SET_FONT_COLOR(236);
				printf(" C%4d", d);
				pos_x = pos_x + 6;
				if (f.fifo_taille == TAILLE_FIFO - 1)
				{
					if (pos_x > MAX_CARA_LIGNE) {pos_x = 1; pos_y = pos_y+1;}
					SET_CURSOR_POSITION(pos_y,pos_x);
					SET_BACKGROUND_COLOR(208);
					SET_FONT_COLOR(236);
					printf(" CrevP");
					pos_x = pos_x + 6;
					reveille(1);
				}
				_unlock_();
			}
		}
	}
}



int main()
{
  usart_init(115200);
  CLEAR_SCREEN(1);
  SET_CURSOR_POSITION(1,1);
  test_colors();
  CLEAR_SCREEN(1);
  SET_CURSOR_POSITION(1,1);
  puts("Test noyau");
  puts("Noyau preemptif");
  SET_CURSOR_POSITION(5,1);
  SAVE_CURSOR_POSITION();
  start(tacheA);
  return(0);
}
