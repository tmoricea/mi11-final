/*
 * OSQueue.h
 *
 *  Created on: 21 juin 2023
 *      Author: tom02
 */

#ifndef PROJET_OSQUEUE_H_
#define PROJET_OSQUEUE_H_

#include "MEM.h"
#include "../Semaphore/SEM.h"
#include <stdlib.h>

#define OSQUEUE_SZ 256

#define NO_MSG -1
#define QUEUE_FULL -2

typedef struct MEM_PART MEM_PART;

typedef struct {
    MEM_PART* tab[OSQUEUE_SZ];
    uint8_t osqueue_size;
    uint8_t osqueue_tete;
    uint8_t osqueue_queue;
} OSQUEUE;

void OSQCreate();
void OSQFlush();
int16_t OSQPost(MEM_PART* msg);
void QPend(MEM_PART** out);
int16_t QAccept(MEM_PART** out);
uint8_t QQuery();

#endif /* PROJET_OSQUEUE_H_ */
