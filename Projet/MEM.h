/*
 * MEM.h
 *
 *  Created on: 21 juin 2023
 *      Author: tom02
 */

#ifndef PROJET_MEM_H_
#define PROJET_MEM_H_

#include <stdint.h>
#include "../Semaphore/SEM.h"

#define MEM_PART_SZ 32
#define MEM_PART_C 128

#define MEM_PART_TOTAL_SZ MEM_PART_SZ * sizeof(MEM_PART)

typedef struct MEM_PART {
	uint8_t owned;
	uint8_t data[MEM_PART_SZ];
} MEM_PART;

void MemCreate();
int16_t MemGet(MEM_PART** dst);
void MemPut(MEM_PART* ptr);
uint8_t MemQuery();

#endif /* PROJET_MEM_H_ */
