/*
 * OSQueue.c
 *
 *  Created on: 21 juin 2023
 *      Author: tom02
 */


#include "OSQueue.h"
#include "../Communs/serialio.h"

OSQUEUE _msg_queue;
uint8_t _mutex_msg_queue;
uint8_t _alarm_msg_queue;

void OSQCreate() {
	_msg_queue.osqueue_queue = _msg_queue.osqueue_tete = _msg_queue.osqueue_size = 0;
	for(uint32_t i = 0 ; i<OSQUEUE_SZ ; i++) {
		_msg_queue.tab[i] = NULL;
	}

	_mutex_msg_queue = s_cree(1);
	_alarm_msg_queue = s_cree(0);
}

void OSQFlush() {
	_msg_queue.osqueue_queue = _msg_queue.osqueue_tete = _msg_queue.osqueue_size = 0;
	for(uint32_t i = 0 ; i<OSQUEUE_SZ ; i++) {
		_msg_queue.tab[i] = NULL;
	}
}

/**
 * Ajoute un élément
 * @param msg
 * @return
 */
int16_t OSQPost(MEM_PART* msg) {
	s_wait(_mutex_msg_queue);

	int16_t ret = 0;

	_msg_queue.tab[_msg_queue.osqueue_queue++] = msg;
	_msg_queue.osqueue_queue %= OSQUEUE_SZ;
	_msg_queue.osqueue_size++;

	if(_msg_queue.osqueue_queue == _msg_queue.osqueue_tete) {
		_msg_queue.osqueue_tete++;
		_msg_queue.osqueue_tete %= OSQUEUE_SZ;
		ret = QUEUE_FULL;
	}

	s_signal(_mutex_msg_queue);
	s_signal(_alarm_msg_queue);

	return ret;
}

/**
 * Défile un message (bloquant)
 * @param msg
 * @return
 */
void QPend(MEM_PART** out) {
	s_wait(_alarm_msg_queue);
	s_wait(_mutex_msg_queue);

	*out = _msg_queue.tab[_msg_queue.osqueue_tete];
	_msg_queue.osqueue_tete++;
	_msg_queue.osqueue_tete %= OSQUEUE_SZ;

	s_signal(_mutex_msg_queue);
}

/**
 * Défile un message (non bloquant)
 * @param msg
 * @return
 */
int16_t QAccept(MEM_PART** out) {
	s_wait(_mutex_msg_queue);

	int16_t ret = NO_MSG;
	if(QQuery()) {
		*out = _msg_queue.tab[_msg_queue.osqueue_tete];
		_msg_queue.osqueue_tete++;
		_msg_queue.osqueue_tete %= OSQUEUE_SZ;
		ret = 0;
	}

	s_signal(_mutex_msg_queue);

	return ret;
}

uint8_t QQuery() {
	return _msg_queue.osqueue_size > 0;
}
