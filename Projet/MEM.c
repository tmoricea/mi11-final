/*
 * MEM.c
 *
 *  Created on: 21 juin 2023
 *      Author: tom02
 */


#include "MEM.h"

MEM_PART _partition_mem[MEM_PART_C];
uint8_t _mutex_mem;

void MemCreate() {
	for(uint32_t i = 0 ; i < MEM_PART_C ; i++) {
		_partition_mem[i].owned = 0;
	}

	_mutex_mem = s_cree(1);
}

int16_t MemGet(MEM_PART** dst) {
	s_wait(_mutex_mem);

	for(uint32_t i = 0 ; i < MEM_PART_C ; i++) {
		if(!_partition_mem[i].owned) {
			_partition_mem[i].owned = 1;
			*dst = &_partition_mem[i];
			s_signal(_mutex_mem);
			return 0;
		}
	}
	s_signal(_mutex_mem);

	return -1;
}

void MemPut(MEM_PART* ptr) {
	ptr->owned = 0;
	for(uint32_t i = 0 ; i < MEM_PART_SZ ; i++) {
		ptr->data[i] = 0;
	}
}

uint8_t MemQuery() {
	for(uint32_t i = 0 ; i < MEM_PART_C ; i++) {
		if(!_partition_mem[i].owned)
			return 1;
	}

	return 0;
}
