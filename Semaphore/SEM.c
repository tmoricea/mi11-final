/*----------------------------------------------------------------------------*
 * fichier : sem.c                                                            *
 * gestion des semaphores pour le mini-noyau temps reel                       *
 *----------------------------------------------------------------------------*/

#include "../Semaphore/SEM.h"
#include "../Communs/FIFO.h"
#include "../TP1-2/noyau.h"

/*----------------------------------------------------------------------------*
 * declaration des structures                                                 *
 *----------------------------------------------------------------------------*/

/*
 * structure definissant un semaphore
 */
#define N_OWNED 0b1000000
#define N_SWAPED 0b1000000

typedef struct {
    FIFO file;
    int8_t valeur;
    uint16_t owner;
    uint16_t swaped_with;
    uint8_t prio;
} SEMAPHORE;

/*----------------------------------------------------------------------------*
 * variables globales internes                                                *
 *----------------------------------------------------------------------------*/

/*
 * variable stockant tous les semaphores du systeme
 */
SEMAPHORE _sem[MAX_SEM];

/*----------------------------------------------------------------------------*
 * definition des fonctions                                                   *
 *----------------------------------------------------------------------------*/

/*
 * /!!!!\ NOTE IMPORTANTE /!!!!\
 * pour faire les verifications de file, on pourra utiliser la variable de
 * file fifo_taille et la mettre a -1 dans le cas ou la file n'est pas
 * utilisee
 */

/*
 * initialise les sempaphore du systeme
 * entre  : sans
 * sortie : sans
 * description : initialise l'ensemble des semaphores du systeme
 */
void s_init(void) {
	register SEMAPHORE *s = _sem;
	register unsigned j;
		   
   
	for (j = 0; j < MAX_SEM; j++)
	{
		s->file.fifo_taille = -1;
		s->owner = N_OWNED;
		s->swaped_with = N_SWAPED;
		s->prio = MAX_FILE - 1;
		s++;
	}
}

/*
 * cree un semaphore
 * entre  : valeur du semaphore a creer
 * sortie : numero du semaphore cree
 * description : cree un semaphore
 *               en cas d'erreur, le noyau doit etre arrete
 */
uint8_t s_cree(int8_t valeur) {
	register SEMAPHORE *s = _sem;
	register unsigned n = 0;

	_lock_();
	/* Rechercher un sem libre */
	while(s->file.fifo_taille != 255 && n < MAX_SEM)
	{
		n++;
		s++;
	}

	if (n < MAX_SEM)
	{
		fifo_init(&(s->file));
		s->valeur = valeur;
	}
	else
	{
		noyau_exit();
	}
	_unlock_();

	return(n);	   
}

/*
 * ferme un semaphore pour qu'il puisse etre reutilise
 * entre  : numero du semaphore a fermer
 * sortie : sans
 * description : ferme un semaphore
 *               en cas d'erreur, le noyau doit etre arrete
 */
void s_close(uint8_t n) {
	register SEMAPHORE *s = &_sem[n];

	_lock_();

	/* V‚rifier sem cr‚e */
	if (s->file.fifo_taille == -1)
	{
		noyau_exit();
	}

	s->file.fifo_taille = -1;
	_unlock_();		   
}

/*
 * tente de prendre le semaphore dont le numero est passe en parametre
 * entre  : numero du semaphore a prendre
 * sortie : sans
 * description : prend le semaphore
 *               si echec, la tache doit etre suspendue
 *               en cas d'erreur, le noyau doit etre arrete
 */
void s_wait(uint8_t n) {

	register SEMAPHORE *s = &_sem[n];

	_lock_();

	if (s->file.fifo_taille == -1)
	{
		noyau_exit();
	}

	s->valeur--;

	NOYAU_TCB* tcb_owner = noyau_get_p_tcb(s->owner);
	NOYAU_TCB* tcb_current = noyau_get_p_tcb(noyau_get_tc());
	if(s->prio > tcb_current->prio) {
		s->prio = tcb_current->prio;
	}

	if (s->valeur < 0)
	{
		if(tcb_owner->prio > s->prio) {
			set_prio(s->owner, s->prio);
		}
		fifo_ajoute(&s->file, noyau_get_tc());
		dort();
	} else {
		s->owner = noyau_get_tc();
	}

	_unlock_();		
}

/*
 * libere un semaphore
 * entre  : numero du semaphore a liberer
 * sortie : sans
 * description : libere un semaphore
 *               si des taches sont en attentes, la premiere doit etre reveillee
 *               en cas d'erreur, le noyau doit etre arrete
 */
void s_signal(uint8_t n) {
	register SEMAPHORE *s = &_sem[n];
	uint8_t t;

	_lock_();

	if (s->file.fifo_taille == -1)
	{
		noyau_exit();
	}

	s->valeur++;

	NOYAU_TCB* tcb_current = noyau_get_p_tcb(noyau_get_tc());
	//set_prio(tcb_current->id, tcb_current->id>>3);

	if (s->valeur <= 0)
	{
		set_prio(s->owner, s->owner>>3);

		uint16_t max_prio = MAX_FILE - 1;
		for(uint8_t i = s->file.fifo_tete ; i%TAILLE_FIFO != s->file.fifo_queue ; i++) {
			if(s->file.tab[i]>>3 < max_prio) max_prio = s->file.tab[i]>>3;
		}
		s->prio = max_prio;

		fifo_retire(&s->file, (uint8_t *) &t);
		s->owner = t;
		set_prio(s->owner, s->prio);

		reveille(t);
	} else {
		s->owner = N_OWNED;
		s->prio = MAX_FILE - 1;
	}
	_unlock_();	
}





