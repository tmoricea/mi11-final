/*----------------------------------------------------------------------------*
 * fichier : PCS.c                                                            *
 * programme de test des sémaphore sur Prod Cons                              *
 *----------------------------------------------------------------------------*/

#include <stdint.h>
#include <stdlib.h>

#include "../TP1-2/noyau.h"
#include "../Communs/stm32h7xx.h"
#include "../Communs/serialio.h"
#include "../Communs/TERMINAL.h"
#include "../Communs/FIFO.h"
#include "../Semaphore/SEM.h"

TACHE	TacheA(void);
TACHE	Prod(void);
TACHE	Cons(void);

uint8_t ProdID;
uint8_t ConsID;
uint8_t mutex;
uint8_t Csem;
uint8_t Psem;


FIFO f;

TACHE Prod( void )
{

	uint8_t d = 0;


	while(d < 256)
	{
		s_wait(mutex);
		if (!fifo_ajoute(&f, d))
		{
			s_signal(mutex);
			s_wait(Psem);
		}
		else
		{
			d++;
			if (f.fifo_taille == 1)
			{
				s_signal(Csem);
			}
			s_signal(mutex);
		}
	}
	fin_tache();
}

TACHE Cons( void )
{
	uint8_t d;

	while(1)
	{
		s_wait(mutex);
		if (!fifo_retire(&f, (uint8_t *) &d))
		{
			s_signal(mutex);
			s_wait(Csem);
		}
		else
		{
			printf("%03d |", d);
			if (f.fifo_taille == TAILLE_FIFO - 1)
			{
				s_signal(Psem);
			}
			s_signal(mutex);
		}
	}
}


TACHE TacheA( void )
{

	fifo_init(&f);
	mutex = s_cree(1);
	Csem = s_cree(0);
	Psem = s_cree(0);

	active(cree(Cons));
    active(cree(Cons));
	active(cree(Prod));
    active(cree(Prod));
	fin_tache();
}


int main()
{
	usart_init(115200);
	puts("Test noyau");
	puts("Noyau preemptif");
	start(TacheA);
	getchar();
	return(0);
}
